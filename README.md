# Compsass

This is the baseline of what at some point will be a mixin library to replace compass in our toolchain, when used with autoprefixer. If you don't like it or nobody is willing to support it anymore, just revert to compass.

First thing, find a better name.

## Usage

Copy _compass_ to your root sass directory. For now you have to make `@import "compass"` to include the helper functions. This will be removed at a later date, once I decide on how to manage dependencies.

## Contributing

* Keep folder structure according to compass components so that `@import "compass/css3/animation"` will still work.
* don't write any prefix code
* steal from official compass whatever we can.
* _compass/helpers_ contains functions which normally are handled by ruby.
* config.rb variables are in _compass/config_

### Stolen files

* reset/*
* _reset.scss

### Modified Stolen files

* css3/_font-face.scss
